let
  sources = import ./sources.nix;

  ghc-overrides = pkgs: self: super:
    let hs = pkgs.haskell.lib;
    in {
      bitcoin-address = super.callPackage ../bitcoin-address/pkg.nix { };

      bitcoin-script =
        super.callPackage "${sources.haskell-bitcoin-script}/bitcoin-script.nix"
        { };
      bitcoin-hash =
        super.callPackage "${sources.hs-bitcoin-hash}/bitcoin-hash/pkg-ghc.nix"
        { };
      bitcoin-keys =
        super.callPackage "${sources.hs-bitcoin-keys}/bitcoin-keys/pkg-ghc.nix"
        { };
      secp256k1-haskell =
        hs.addPkgconfigDepend super.secp256k1-haskell pkgs.secp256k1;

      _shell = super.shellFor {
        withHoogle = false;
        packages = p: [ p.bitcoin-address ];
        nativeBuildInputs = [ pkgs.cabal-install ];
      };
    };

  ghcjs-overrides = pkgs: self: super:
    let hs = pkgs.haskell.lib;
    in {
      bitcoin-address =
        hs.doCheck (super.callPackage ../bitcoin-address/pkg.nix { });

      bitcoin-script = hs.doCheck (super.callPackage
        "${sources.haskell-bitcoin-script}/bitcoin-script.nix" { });
      bitcoin-hash = hs.doCheck (super.callPackage
        "${sources.hs-bitcoin-hash}/bitcoin-hash/pkg-ghcjs.nix" { });
      bitcoin-keys = hs.doCheck (super.callPackage
        "${sources.hs-bitcoin-keys}/bitcoin-keys/pkg-ghcjs.nix" { });
      extra = hs.dontCheck super.extra;
      quickcheck-assertions = hs.dontCheck super.quickcheck-assertions;
      QuickCheck = hs.dontCheck super.QuickCheck;
      tasty-quickcheck = hs.dontCheck super.tasty-quickcheck;
      terminal-size =
        super.callCabal2nix "terminal-size" sources.terminal-size { };
      time-compat = hs.dontCheck super.time-compat;

      _shell = super.shellFor {
        withHoogle = false;
        packages = p: [ p.bitcoin-address ];
        nativeBuildInputs = [ pkgs.nodejs pkgs.cabal-install ];
      };
    };

  pkgs-overlay = self: super: {
    _here = {
      ghc883 = super.haskell.packages.ghc883.override {
        overrides = ghc-overrides self;
      };
      ghc865 = super.haskell.packages.ghc865.override {
        overrides = ghc-overrides self;
      };
      ghcjs86 = super.haskell.packages.ghcjs.override {
        overrides = ghcjs-overrides self;
      };
    };
  };

in import sources.nixpkgs { overlays = [ pkgs-overlay ]; }
