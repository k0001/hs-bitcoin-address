let pkgs = import ./nix;
in rec {
  ghc883 = pkgs._here.ghc883.bitcoin-address;
  ghc865 = pkgs._here.ghc865.bitcoin-address;
  ghcjs86 = pkgs._here.ghcjs86.bitcoin-address;
}
