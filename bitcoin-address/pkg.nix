{ mkDerivation, base, base16-bytestring, base58-bytestring, bech32, bitcoin-keys
, bitcoin-hash, bitcoin-script, bytestring, hedgehog, stdenv, tasty, text
, tasty-hedgehog, tasty-hunit, binary, nix-gitignore
}:
mkDerivation {
  pname = "bitcoin-address";
  version = "0.1";
  src = nix-gitignore.gitignoreSourcePure ../.gitignore ./.;
  libraryHaskellDepends = [
    base base58-bytestring bech32 bitcoin-hash bitcoin-script bitcoin-keys
    bytestring binary text
  ];
  testHaskellDepends = [
    base base16-bytestring bytestring hedgehog tasty tasty-hedgehog
    tasty-hunit
  ];
  homepage = "https://gitlab.com/k0001/hs-bitcoin-address";
  description = "Render and parse Bitcoin (BTC) addresses";
  license = stdenv.lib.licenses.asl20;
}
