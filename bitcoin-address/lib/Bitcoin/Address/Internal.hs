{-# OPTIONS_HADDOCK hide #-}
module Bitcoin.Address.Internal
 ( op0to16
 , scriptBytes
 , hush
 ) where

import qualified Data.Binary as Bin
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.Bitcoin.Script as S

--------------------------------------------------------------------------------

-- | Converts the numbers [0 … 16] to the corresponding ['S.OP_0' … 'S.OP_16'].
op0to16 :: Int -> Maybe S.ScriptOp
op0to16 i = case i of
  0  -> Just S.OP_0
  1  -> Just S.OP_1
  2  -> Just S.OP_2
  3  -> Just S.OP_3
  4  -> Just S.OP_4
  5  -> Just S.OP_5
  6  -> Just S.OP_6
  7  -> Just S.OP_7
  8  -> Just S.OP_8
  9  -> Just S.OP_9
  10 -> Just S.OP_10
  11 -> Just S.OP_11
  12 -> Just S.OP_12
  13 -> Just S.OP_13
  14 -> Just S.OP_14
  15 -> Just S.OP_15
  16 -> Just S.OP_16
  _  -> Nothing

-------------------------------------------------------------------------------

-- | Obtain the raw 'S.Script' bytes.
scriptBytes :: S.Script -> B.ByteString
scriptBytes = BL.toStrict . Bin.encode
{-# INLINE scriptBytes #-}

--------------------------------------------------------------------------------

hush :: Either a b -> Maybe b
hush = either (\_ -> Nothing) Just
{-# INLINE hush #-}

